# Ansible Role Minion

This role creates the user `minion`, firewall rules as well as a dedicated network interface for rootless container workloads in CoreOS and other ostree based goodness.

# TODO
- add port 80 and 69 for ansible_role_pxe (http and tftp) ... how can we specify those ports in inventory?

# Debug Notes
```
$ export XDG_RUNTIME_DIR=/run/user/$(id -u)
$ podman network create podman-dedicated --interface-name=macvtap0
$ podman run -it --rm --network podman-dedicated alpine ip addr
``` 


# Requirements
- sudo on the node
